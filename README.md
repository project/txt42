## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration

## INTRODUCTION

Txt42 is a ChatGPT-based AI co-writing tool for CKEditor and Drupal. 

This is a premium plugin and uses an external service (OpenAI, the developer of ChatGPT), so there is no free tier.

It will help you to upload, choose and manage files and also edit your images.
This module includes a **CKEditor 4** plugin so will perfectly integrate into your
Drupal installation.

Official website: [txt42.ai](https://txt42.ai)

## REQUIREMENTS

There are no special requirements, except CKEditor 4.
Txt42 acts as its add-on for CKEditor 4.

## INSTALLATION

Just install this module as any other module for Drupal (via Composer). Follow the standard Drupal module installation manuals.

## CONFIGURATION

The module comes already preconfigured to be used after installation.
If you want to change any options, do it as usual for the text format you need on:

  `Configuration > Content authoring > Text formats`

choose and open the text format you want to configure, then will see all
Txt42 available options inside CKEditor configuration widget.
Press "Save" after editing something there.

## MAINTAINERS

- [Dmitriy Komarov](https://drupal.org/u/dmitriy-komarov)
- [Olga Tarkova](https://www.drupal.org/u/olga-tarkova)